// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false, 
   firebaseConfig : {
    apiKey: "AIzaSyDPaZWw1-sUewzj-kG1s7Ku-14rk-lLrXM",
    authDomain: "plussoft-20e27.firebaseapp.com",
    databaseURL: "https://plussoft-20e27.firebaseio.com",
    projectId: "plussoft-20e27",
    storageBucket: "plussoft-20e27.appspot.com",
    messagingSenderId: "793317462032",
    appId: "1:793317462032:web:0dd559b9aa04034b10ba95",
    measurementId: "G-QQMYXNKE1V"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
