import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, from } from 'rxjs';
import { ConexionService } from '../services/conexion.service';


@Component({
  selector: 'app-entrada',
  templateUrl: './entrada.component.html',
  styleUrls: ['./entrada.component.css']
})
export class EntradaComponent implements OnInit {

  inventario;
  editarItem: any=
  {
    name: ""
  };
  Zapato: any ={
  }
  numeros: number[]= [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32
  ];

  constructor(private conexion:ConexionService ) { 
 this.conexion.ListaZapato().subscribe(Zapato=>
  {
    this.inventario = Zapato;
    console.log(this.inventario);
  })}

  agregar()
  {
    this.conexion.agregarZapato(this.Zapato);
  }
  eliminar(Zapato)
  {
    this.conexion.eliminarZapato(Zapato);
  }
  editar(Zapato)
  {
    this.editarItem= Zapato;
  }
  agregarItemEditado()
  {
    this.conexion.editarZapato(this.editarItem);
  }

  ngOnInit() {
    console.log(this.Zapato);
  }

}
