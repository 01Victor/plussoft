import { Injectable } from '@angular/core';

@Injectable()
export class TipoService {
  
  contador : number = 0;
  
  constructor() { }

  public incrementValue(){
    console.log('Contador aumentando');
    this.contador++;
  }
}