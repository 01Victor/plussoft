import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { ActivatedRoute} from '@angular/router'
import { ConexionService } from '../services/conexion.service';
import { Observable, from } from 'rxjs';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

  public id: string;
  inventario;
  Zapato: any ={
  };


pageTitle: string = 'Product Detail ';
  constructor(private _route: ActivatedRoute, private conexion:ConexionService) {
    console.log(this._route.snapshot.paramMap.get('id'));

    this.conexion.ListaZapato().subscribe(Zapato=>
      {
        this.inventario = Zapato;
        this.inventario = this.inventario.filter(zapato => zapato.carrito == true)
        console.log(this.inventario);
      })
   }

  ngOnInit() {
    this.pageTitle +=`:${this.id}`
    
  }
  
  quitarCarro(zapa)
  {
zapa.cantidad = zapa.cantidad+1;
zapa.carrito = false;
this.conexion.editarZapato(zapa);
  }

}
