import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import {HttpClientModule} from '@angular/common/http'
import {UsuariosService} from '../app/usuarios.service'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { ConexionService} from './services/conexion.service';
import { AuthService} from './services/auth.service'
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { InicioComponent } from './inicio/inicio.component';
import { GeneroComponent } from './genero/genero.component';
import { TipoComponent } from './tipo/tipo.component';
import { ResultadosComponent } from './resultados/resultados.component';
import { ReporteComponent } from './reporte/reporte.component';
import { CarritoComponent } from './carrito/carrito.component';
import { from } from 'rxjs';
import { EntradaComponent } from './entrada/entrada.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { IconocarroComponent } from './iconocarro/iconocarro.component';
import { RegistroPagoComponent } from './registro-pago/registro-pago.component';
import { MarcasComponent } from './marcas/marcas.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { TiposComponent } from './tipos/tipos.component';
import { GenerosComponent } from './generos/generos.component';
import { FiltromarcasComponent } from './filtromarcas/filtromarcas.component';
import { PagoComponent } from './pago/pago.component';

@NgModule({
  declarations: [
    AppComponent, LoginComponent, InicioComponent, GeneroComponent, TipoComponent,
     ResultadosComponent, ReporteComponent, CarritoComponent, RegistrarComponent, MarcasComponent, EntradaComponent,
     RegistroPagoComponent, IconocarroComponent, NotFoundComponent, TiposComponent, GenerosComponent, FiltromarcasComponent, PagoComponent
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'my-app-name'), // imports firebase/app needed for everything
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule // imports firebase/storage only needed for storage features
    ,AngularFontAwesomeModule
  ],

  providers: [UsuariosService, ConexionService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
