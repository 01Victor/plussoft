import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  name: string;
  position: string;
  weight: string;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 'EJ349J', name: 'Nike', weight: 'Rose Gold', symbol: 'any.jpg'},
  {position: 'JI98EK', name: 'Nike', weight: 'Running', symbol: 'any.jpg'},
  {position: 'BN82P3', name: 'Nike', weight: 'Nice Day', symbol: 'any.jpg'},
  {position: 'ED5RFT', name: 'Nike', weight: 'Funny', symbol: 'any.jpg'},
  {position: 'EDF543', name: 'Nike', weight: 'Hail Climb', symbol: 'any.jpg'},
  {position: 'B5N6KM', name: 'Nike', weight: 'Yellow Yeah', symbol: 'any.jpg'},
  {position: 'P98I0U', name: 'Nike', weight: 'Monday Fit', symbol: 'any.jpg'},
  {position: 'CSAZ34', name: 'Nike', weight: 'Custom Design', symbol: 'any.jpg'},
  {position: 'SDCXWE', name: 'Nike', weight: 'Sad Times', symbol: 'any.jpg'},
  {position: 'dRR23D', name: 'Nike', weight: 'Expert', symbol: 'any.jpg'},
];

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styleUrls: ['./resultados.component.css']
})

export class ResultadosComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
