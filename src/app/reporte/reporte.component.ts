import { Component, OnInit } from '@angular/core';
// import {Sort} from '@angular/material/sort';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, from } from 'rxjs';
import { ConexionService } from '../services/conexion.service';

/* 
export interface Dessert {
  nombre: string;
  ventapieza: number;
  numempl: number;
ventapesos: number;
  bono: number;
} */

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.css']
})
export class ReporteComponent implements OnInit {

  items: any;
  item: any ={
    name: ""
  }
/*   desserts: Dessert[] = [
    { nombre: 'Juan Víctor Amador Acevedo', numempl: 632, ventapieza: 24, ventapesos: 12000, bono: 300},
    { nombre: 'Tania Puentes Méndez', numempl: 633, ventapieza: 15, ventapesos: 7500, bono: 70},
    { nombre: 'Rafael García Molina', numempl: 634, ventapieza: 8, ventapesos: 4000, bono: 30},
    { nombre: 'Humberto López Farías', numempl: 635, ventapieza: 22, ventapesos: 11000, bono: 250},
  ];

  sortedData: Dessert[]; */
  constructor(private conexion:ConexionService ) { 
 this.conexion.ListaItem().subscribe(item=>
  {
    this.items = item;
    console.log(this.items);
  })
    // this.sortedData = this.desserts.slice();
  }
  

  ngOnInit() {
  }
  agregar()
  {
    this.conexion.agregarItem(this.item);
    this.item.name = '';
  }
/*   sortData(sort: Sort) {
    const data = this.desserts.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    } 

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nombre': return compare(a.nombre, b.nombre, isAsc);
        case 'numempl': return compare(a.numempl, b.numempl, isAsc);
        case 'ventapieza': return compare(a.ventapieza, b.ventapieza, isAsc);
        case 'ventapesos': return compare(a.ventapesos, b.ventapesos, isAsc);
        case 'bono': return compare(a.bono, b.bono, isAsc);
        default: return 0;
      }
    });
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

*/}