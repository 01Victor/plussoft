import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { ActivatedRoute} from '@angular/router'
import { ConexionService } from '../services/conexion.service';
import { Observable, from } from 'rxjs';

@Component({
  selector: 'app-filtromarcas',
  templateUrl: './filtromarcas.component.html',
  styleUrls: ['./filtromarcas.component.css']
})
export class FiltromarcasComponent implements OnInit {

  public id: string;
  inventario;
  Zapato: any ={
  };


pageTitle: string = 'Product Detail ';
  constructor(private _route: ActivatedRoute, private conexion:ConexionService) {
    console.log(this._route.snapshot.paramMap.get('id'));

    this.conexion.ListaZapato().subscribe(Zapato=>
      {
        this.inventario = Zapato;
        this.inventario = this.inventario.filter(zapato => zapato.marca == this._route.snapshot.paramMap.get('id'))
        console.log(this.inventario);
      })
   }

  ngOnInit() {
    console.log(this._route.snapshot.paramMap.get('id'));
    this.pageTitle +=`:${this.id}`
  }
  agCarro(zapa)
  {
zapa.cantidad = zapa.cantidad-1;
zapa.carrito = true;
this.conexion.editarZapato(zapa);
  }

}
