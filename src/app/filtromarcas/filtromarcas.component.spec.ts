import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltromarcasComponent } from './filtromarcas.component';

describe('FiltromarcasComponent', () => {
  let component: FiltromarcasComponent;
  let fixture: ComponentFixture<FiltromarcasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltromarcasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltromarcasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
