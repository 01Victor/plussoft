import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, from } from 'rxjs';
import { ConexionService } from '../services/conexion.service';


@Component({
  selector: 'app-tipo',
  templateUrl: './tipo.component.html',
  styleUrls: ['./tipo.component.css']
})
export class TipoComponent implements OnInit {

  inventario;
  Zapato: any ={
  };

  constructor(private conexion:ConexionService) {
    this.conexion.ListaZapato().subscribe(Zapato=>
      {
        this.inventario = Zapato;
        this.inventario = this.inventario.filter(zapato => zapato.tipo =="Zapato")
        console.log(this.inventario);
      })
   }

  ngOnInit() {
   console.log(this.Zapato);
  }

}
