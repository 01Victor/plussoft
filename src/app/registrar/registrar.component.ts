import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service'
import { from } from 'rxjs';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  public email: string;
  public password: string;

  constructor(
  public authService: AuthService
  ) { }

  ngOnInit() {
  }

  onSubmitAddUser()
{
  console.log("bien");
  this.authService.registerUser(this.email, this.password)
.then( (res) =>{
  console.log('Bien Hecho');
  console.log(res);
  this.email= "";
  this.password = "";
}).catch( (err)=>{
  console.log(err);
});

}

}
