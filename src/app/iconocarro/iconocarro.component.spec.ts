import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconocarroComponent } from './iconocarro.component';

describe('IconocarroComponent', () => {
  let component: IconocarroComponent;
  let fixture: ComponentFixture<IconocarroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconocarroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconocarroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
