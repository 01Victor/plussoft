import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service'
import { from } from 'rxjs';
import { auth } from 'firebase';
import { Router } from '@angular/router';
import { RouterModule, RouterLink } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  public isLogin: boolean;

  public nombreUsuario: string;
  public emailUsuario: string;

  constructor(public authService: AuthService, private Router: Router
  ) {

  }

  ngOnInit() {
    console.log(this.isLogin);
    this.authService.getAuth().subscribe(auth => {
      if (auth) {
        this.isLogin = true;
        this.nombreUsuario = auth.displayName;
        this.emailUsuario = auth.email;
      }
      else {
        this.isLogin = false;
      }
    })
  }
  onClickLogout() {
    this.authService.logout();
    console.log(this.isLogin);
    this.Router.navigate(['/login']);

  }
}
