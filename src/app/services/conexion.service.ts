import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Item { name: string, edad:number, ciudad: string; }
export interface Zapato { nombre: string, talla:number, color: string,
  cantidad: number, descuento: number, precio: number, total: number,
   genero: string, tipo: string, marca: string; }

@Injectable({
  providedIn: 'root'  
})
export class ConexionService {

  private itemsCollection: AngularFirestoreCollection<Item>;
  private inventarioCollection: AngularFirestoreCollection<Zapato>;
  
  items: Observable<Item[]>;
  inventario: Observable<Zapato[]>;
  inventario2: Observable<Zapato[]>;

  private itemDoc: AngularFirestoreDocument<Zapato>;

  constructor(private afs: AngularFirestore) { 

    this.itemsCollection = afs.collection<Item>('items');
    this.inventarioCollection = afs.collection<Zapato>('inventario');


    this.items = this.itemsCollection.snapshotChanges().pipe(
      map (actions => actions.map(a => {
        const data = a.payload.doc.data() as Item;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );

    this.inventario = this.inventarioCollection.snapshotChanges().pipe(
      map (actions => actions.map(a => {
        const data = a.payload.doc.data() as Zapato;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );

  }

  ListaItem() {
    return this.items;
  }
  ListaZapato() {
    return this.inventario;
  }
  ListaZapatoFiltro(filtro: string) {

 

  }
    agregarItem(item: Item) {
      this.itemsCollection.add(item);
  }

agregarZapato(zapato: Zapato) {
  this.inventarioCollection.add(zapato);
}

eliminarZapato(zapato)
{
this.itemDoc = this.afs.doc<Zapato>(`inventario/${zapato.id}`);
this.itemDoc.delete();
}
editarZapato(zapato)
{
this.itemDoc = this.afs.doc<Zapato>(`inventario/${zapato.id}`);
this.itemDoc.update(zapato);
}

}
