import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import{ InicioComponent } from './inicio/inicio.component'
import { AppComponent } from './app.component';
import { formatNumber } from '@angular/common';
import { TipoComponent } from './tipo/tipo.component';
import { GeneroComponent } from './genero/genero.component';  
import { ResultadosComponent } from './resultados/resultados.component';
import { ReporteComponent } from './reporte/reporte.component';
import { CarritoComponent } from './carrito/carrito.component';
import { EntradaComponent } from './entrada/entrada.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { MarcasComponent} from './marcas/marcas.component';
import { RegistroPagoComponent} from './registro-pago/registro-pago.component'
import { from } from 'rxjs';
import { NotFoundComponent } from './not-found/not-found.component';
import { TiposComponent } from './tipos/tipos.component'
import { GenerosComponent } from './generos/generos.component'
import { FiltromarcasComponent } from './filtromarcas/filtromarcas.component';
const routes: Routes = [

{
  path: '', component: LoginComponent
},
{
  path: 'login', component: LoginComponent
},
{
  path: 'registrar', component: RegistrarComponent
},
{
  path: 'inicio', component: InicioComponent, children:
  [
    {
      path: 'tipo', component: TipoComponent

    },{
      
      path: 'genero', component: GeneroComponent
    },{
      path: 'resultados', component: ResultadosComponent

    },{
      path: 'marcas', component: MarcasComponent

    },{
      path: 'reporte', component: ReporteComponent

    }
    ,{
      path: 'carrito', component: CarritoComponent

    }, 
    {
      path: 'entrada', component: EntradaComponent
    }, 
    {
      path: 'registro_pago', component: RegistroPagoComponent
    }, 
    {
      path: 'tipos/:id', component: TiposComponent
    }, 
    {
      path: 'generos/:id', component: GenerosComponent
    }, 
    {
      path: 'filtromarcas/:id', component: FiltromarcasComponent
    }, 
  ]
}
/* , 
{
  path: '**', component: NotFoundComponent
} */
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
